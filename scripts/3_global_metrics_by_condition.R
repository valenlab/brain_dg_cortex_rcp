rm(list = ls(all.names = TRUE))
gc(reset = TRUE)

library(ORFik)
library(data.table)
library(ggplot2)
library(ggpubr)
library(rstatix)
library(rxtras)

source("scripts/utils.R")
# 5′ UTR translational efficiency: LEADER LSU FPKM / LEADER RNA FPKM
# Scanning efficiency (SE): SSU FPKM / CDS FPKM
# Initiation rate (IR): LSU CDS FPKM / SSU leader FPKM
# Translational Efficiency: LSU CDS FPKM / RNA CDS FPKM
# 80S ratio is CDS 80S FPKM / uORF 80S FPKM

# we need to compare by condition - global changes to the leaders
comp <- PAIRS
dt <- fread(file.path("tables", "metrics_by_tx.csv"))

# deciding which transcripts are being filtered is done in 2_dteg_analysis
filt_tx <- fread("tables/filtered_tx.csv")$tx_filt
dt <- dt[dt$tx %in% filt_tx, ]

dt$condition <- factor(dt$condition, ordered = T, levels = c("DG", "cortex"), labels = c("DG", "cortex"))
dt$log2_le_te <- log2(dt$leader_te)
dt$log2_te <- log2(dt$te)
dt$log2_rna_fpkm <- log2(dt$rna_fpkm)
dt$log2_lsu_fpkm <- log2(dt$lsu_fpkm)
dt$log2_ssu_fpkm <- log2(dt$ssu_fpkm)
dt$log2_scanning_eff <- log2(dt$scanning_eff)
dt$log2_initiation_rate <- log2(dt$initiation_rate)
dt$log2_scanning_ratio <- log2(dt$scanning_ratio)
dt$log2_ssu_queue <- log2(dt$ssu_queue)
dt$log2_ssu_double <- log2(dt$ssu_double/dt$rna_fpkm)

dt <- dt[dt$log2_rna_fpkm > 1, ]

# let filter for example to only complete cases

# # Arc
# a <- dt[dt$tx == "ENSMUST00000023268", ]
# a$tx <- NULL
# a <- melt(a, id.vars = "condition")
# dcast(a, variable ~ condition)

# # zscore expeirment - Eivind does not think this is ok because we remove what he cares about
# dt_ <- dt[!is.na(dt[["log2_rna_fpkm"]]) & !is.infinite(dt[["log2_rna_fpkm"]]), ]
# dt_[, zscore := zscore(log2_rna_fpkm), by = "condition"]
# p <- ggdensity(dt_, x = "zscore", fill = "condition") +
#   labs(x = "rna [zscore log2 FPKM]", fill = "") + 
#   # geom_vline(data = dt[, .(median = median(get(var), na.rm = T)), by = c("condition", "celltype")], 
#   #   mapping = aes(xintercept = median, color = condition, linetype = condition)) +
#   theme_science() +
#   theme(plot.title = element_text(hjust = 0.5)) +
#   scale_color_manual(values = rxtras:::adj_colorblind_mine) +
#   scale_fill_manual(values = rxtras:::adj_colorblind_mine)

# lets plot SSU vs 80S
p <- ggplot(dt[is.finite(log2_ssu_fpkm) & is.finite(log2_lsu_fpkm), ], 
            aes(x = log2_lsu_fpkm, y = log2_ssu_fpkm)) +
  geom_point() + stat_cor() +
  facet_grid(condition ~ .)  +
  labs(x = "80S [log2 FPKM]", y = "SSU [log2 FPKM]")
as.pngpdf("figures/comparisons/ssu_vs_80S_by_condition", p, width = 7, height = 6)

p <- ggplot(dt[is.finite(log2_ssu_fpkm) & is.finite(log2_lsu_fpkm), ], 
            aes(x = log2_lsu_fpkm, y = log2_ssu_fpkm, color = condition, group = condition)) +
  geom_smooth() +
  labs(x = "80S [log2 FPKM]", y = "SSU [log2 FPKM]", color = "") +
  theme_science() +
  scale_color_manual(values = rxtras:::adj_colorblind_mine)
as.pngpdf("figures/comparisons/ssu_vs_80S_by_condition_smooth", p, width = 7, height = 6)


# lets plot TE vs RNA
p <- ggplot(dt[is.finite(log2_te)], 
            aes(x = rna_fpkm, y = log2_te)) +
  geom_point() +
  facet_grid(condition ~ .) +
  coord_cartesian(xlim = c(0, 1000), 
                  ylim = c(-max(dt$log2_te[is.finite(dt$log2_te)]), 
                           max(dt$log2_te[is.finite(dt$log2_te)]))) +
  labs(x = "RNA [zoomed to 1k FPKM]", y = "TE [log2]")
as.pngpdf("figures/comparisons/TE_vs_RNA_by_condition", p, width = 7, height = 6)

# top 10 transcripts in each tissue by TE
te_dt <- dt[is.finite(dt$te), ]
te_dt <- te_dt[order(te_dt$te, decreasing = T, na.last = T), head(.SD, 10), by = c("condition")]
annot <- fread("tables/cds_annotation.csv")
te_dt$gene_symbol <- annot$gene_symbol[match(te_dt$tx, annot$tx_id)]
fwrite(te_dt,
       "tables/top10tx_by_TE_absolute.csv")

p <- ggplot(dt[is.finite(log2_scanning_eff)], 
            aes(x = rna_fpkm, y = log2_scanning_eff)) +
  geom_point() +
  facet_grid(condition ~ .) +
  coord_cartesian(xlim = c(0, 1000),
                  ylim = c(-max(dt$log2_scanning_eff[is.finite(dt$log2_scanning_eff)]), 
                           max(dt$log2_scanning_eff[is.finite(dt$log2_scanning_eff)]))) +
  labs(x = "RNA [zoomed to 1k FPKM]", y = "Scaning Efficiency [log2]")
as.pngpdf("figures/comparisons/SE_vs_RNA_by_condition_zoom", p, width = 7, height = 6)

# dt_s <- tidyr::spread(dt[, c("tx", "condition", "celltype", "log2_le_te")], condition, log2_le_te)
# dt_s$tx <- NULL

ggboxplot_by <- function(dt, var = "log2_le_te", name = "leader TE [log2]", save_name = "leader_TE_vs_condition") {
  dt2 <- dt[, c("tx", "condition", var), with = F][!is.na(dt[[var]]) & !is.infinite(dt[[var]])]
  stat_test <- dt2[, add_significance(adjust_pvalue(t_test(
    .SD, as.formula(paste0(var, " ~ condition")), p.adjust.method = "none"), method = "BH"), "p.adj")]
  y_pos <- dt2[, get_y_position(.SD, as.formula(paste0(var," ~ condition")), scales = "free")]

  stat_test <- merge(stat_test, y_pos, by = c("group1", "group2"))
  
  p <- ggplot(dt[is.finite(dt[[var]]), ], 
              aes(x = condition, y = get(var), color = condition)) +
    geom_segment(data = dt[is.finite(dt[[var]]), ][
      , .(median = median(get(var), na.rm = T)), by = c("condition")],
      aes(y = median, yend = median, x = "cortex", xend = "DG", 
          color = condition, linetype = condition), linewidth = 0.7) +
    geom_boxplot() +
    labs(x = "", y = name,
         title = paste0("#", sum(is.finite(dt[condition == "DG"][[var]])), " genes")) +
    theme(legend.position = "none", plot.title = element_text(hjust = 0.5)) +
    stat_pvalue_manual(
      stat_test,
      step.increase = 0.05,
      tip.length = 0.01,
      bracket.nudge.y = 1,
      label = "p.adj.signif") +
    scale_color_manual(values = COLOR_SCHEME, breaks = names(COLOR_SCHEME))
  as.pngpdf(paste0("figures/comparisons/", save_name), p, width = 7, height = 6)
}

ggdensity_by <- function(dt, var = "log2_le_te", name = "leader TE [log2]", 
                         save_name = "leader_TE_vs_condition_density") {
  dt <- dt[!is.na(dt[[var]]) & !is.infinite(dt[[var]]), ]
  stat_test <- add_significance(rstatix::kruskal_test(formula = as.formula(paste0(var, " ~ condition")),
                                                      data = dt))
  y_pos <- dt[, get_y_position(.SD, as.formula(paste0(var," ~ condition")), scales = "free")]
  stat_test <- cbind(stat_test, y_pos)
  stat_test$text <- paste0("\n   ", stat_test$method, " = ", round(stat_test$statistic, 0), 
                           ", df = ", stat_test$df, ", ", stat_test$p.signif)
  
  stat_test <- merge(stat_test, y_pos, by = c("group1", "group2"))
  
  p <- ggdensity(dt, x = var, fill = "condition") +
    labs(x = name, fill = "") + 
    geom_vline(data = dt[, .(median = median(get(var), na.rm = T)), by = c("condition")],
      mapping = aes(xintercept = median, color = condition), show.legend = F) +
    theme_science() +
    theme(plot.title = element_text(hjust = 0.5)) +
    scale_color_manual(values = COLOR_SCHEME, breaks = names(COLOR_SCHEME)) +
    scale_fill_manual(values = COLOR_SCHEME, breaks = names(COLOR_SCHEME)) +
    annotate(geom = "text", x = -Inf, y = Inf, label = stat_test$text, hjust = 0, vjust = 1)
  as.pngpdf(paste0("figures/comparisons/", save_name), p, width = 10, height = 6) 
}

ggdensity_by(dt)
ggboxplot_by(dt)

ggboxplot_by(dt, "log2_ssu_double", "SSU double / RNA [log2]", "ssu_double_vs_condition")
ggdensity_by(dt, "log2_ssu_double", "SSU double / RNA [log2]", "ssu_double_vs_condition_density")

ggboxplot_by(dt, "log2_ssu_queue", "SSU queued ratio [log2]", "ssu_queue_vs_condition")
ggdensity_by(dt, "log2_ssu_queue", "SSU queued ratio [log2]", "ssu_queue_vs_condition_density")

ggboxplot_by(dt, "log2_rna_fpkm", "RNA FPKM [log2]", "rna_vs_condition")
ggdensity_by(dt, "log2_rna_fpkm", "RNA FPKM [log2]", "rna_vs_condition_density")

ggboxplot_by(dt, "log2_ssu_fpkm", "SSU FPKM [log2]", "ssu_vs_condition")
ggdensity_by(dt, "log2_ssu_fpkm", "SSU FPKM [log2]", "ssu_vs_condition_density")

ggboxplot_by(dt, "log2_lsu_fpkm", "80S FPKM [log2]", "lsu_vs_condition")
ggdensity_by(dt, "log2_lsu_fpkm", "80S FPKM [log2]", "lsu_vs_condition_density")

ggboxplot_by(dt, "log2_te", "TE [log2]", "TE_vs_condition")
ggdensity_by(dt, "log2_te", "TE [log2]", "TE_vs_condition_density")

ggboxplot_by(dt, "log2_scanning_eff", "Scanning efficiency [log2]", 
             "scanning_eff_vs_condition")
ggdensity_by(dt, "log2_scanning_eff", "Scanning efficiency [log2]", 
             "scanning_eff_vs_condition_density")

ggboxplot_by(dt, "log2_scanning_ratio", "Scanning ratio [log2]", 
             "scanning_ratio_vs_condition")
ggdensity_by(dt, "log2_scanning_ratio", "Scanning ratio [log2]", 
             "scanning_ratio_vs_condition_density")

ggboxplot_by(dt, "log2_initiation_rate", "Initiation rate [log2]", 
             "initiation_rate_vs_condition")
ggdensity_by(dt, "log2_initiation_rate", "Initiation rate [log2]", 
             "initiation_rate_vs_condition_density")

ggboxplot_by(dt, "entropy", "Entropy", 
             "entropy_vs_condition")
ggdensity_by(dt, "entropy", "Entropy", 
             "entropy_vs_condition_density")

# uORF
filt_uORF <- fread("tables/filtered_uORF.csv")

uORF_dt <- fread(file.path("tables", "metrics_by_uORF.csv"))
uORF_dt <- uORF_dt[uORF_dt$uORF %in% filt_uORF$uORF_id, ]

uORF_dt$condition <- factor(uORF_dt$condition, ordered = T, 
                            levels = c("DG", "cortex"), labels = c("DG", "cortex"))
uORF_dt$log2_uORF_40S_cons_rate <- log2(uORF_dt$uORF_40S_cons_rate)
uORF_dt$log2_uORF_stop_recognition_rate <- log2(uORF_dt$uORF_stop_recognition_rate)
uORF_dt$log2_uORF_readthrough_rate <- log2(uORF_dt$uORF_readthrough_rate)
uORF_dt$log2_uORF_TE <- log2(uORF_dt$uORF_TE)
uORF_dt$log2_uORF_80S_ratio <- log2(uORF_dt$uORF_80S_ratio)
uORF_dt$tx <- uORF_dt$uORF
uORF_dt$uORF <- NULL


ggboxplot_by(uORF_dt, "log2_uORF_TE", "uORF TE [log2]", 
             "uORF_TE_vs_condition")
ggdensity_by(uORF_dt, "log2_uORF_TE", "uORF TE [log2]", 
             "uORF_TE_vs_condition_density")

ggboxplot_by(uORF_dt, "log2_uORF_80S_ratio", "CDS / uORF 80S ratio [log2]", 
             "uORF_80S_ratio_vs_condition")
ggdensity_by(uORF_dt, "log2_uORF_80S_ratio", "CDS / uORF 80S ratio [log2]", 
             "uORF_80S_ratio_vs_condition_density")

ggboxplot_by(uORF_dt, "log2_uORF_40S_cons_rate", "uORF 40S consumption rate [log2]", 
             "uORF_40S_cons_rate_vs_condition")
ggdensity_by(uORF_dt, "log2_uORF_40S_cons_rate", "uORF 40S consumption rate [log2]", 
             "uORF_40S_cons_rate_vs_condition_density")

ggboxplot_by(uORF_dt, "log2_uORF_stop_recognition_rate", "uORF stop recognition rate [log2]", 
             "uORF_stop_recognition_rate_vs_condition")
ggdensity_by(uORF_dt, "log2_uORF_stop_recognition_rate", "uORF stop recognition rate [log2]", 
             "uORF_stop_recognition_rate_vs_condition_density")

ggboxplot_by(uORF_dt, "log2_uORF_readthrough_rate", "uORF readthrough rate [log2]", 
             "uORF_readthrough_rate_vs_condition")
ggdensity_by(uORF_dt, "log2_uORF_readthrough_rate", "uORF readthrough rate [log2]", 
             "uORF_readthrough_rate_vs_condition_density")