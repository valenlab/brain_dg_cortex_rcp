rm(list = ls(all.names = TRUE))
gc(reset = TRUE)

library(ORFik)
library(data.table)
library(ggplot2)
library(ggpubr)
library(rstatix)
library(rxtras)

cfg <- read.experiment("Preeti_Brain_DG_Cortex_04_2023")
source("./scripts/utils.R")
clive <- clive_groupings()
dir.create("figures/Clive/metrics/", recursive = TRUE, showWarnings = FALSE)

# we need to compare by condition - global changes to the leaders
comp <- PAIRS
dt <- fread(file.path("tables", "metrics_by_tx.csv"))

# deciding which transcripts are being filtered is done in 2_dteg_analysis
filt_tx <- fread("tables/filtered_tx.csv")$tx_filt
dt <- dt[dt$tx %in% filt_tx, ]

dt$condition <- factor(dt$condition, ordered = T, levels = c("DG", "cortex"), labels = c("DG", "cortex"))
dt$log2_le_te <- log2(dt$leader_te)
dt$log2_te <- log2(dt$te)
dt$log2_rna_fpkm <- log2(dt$rna_fpkm)
dt$log2_lsu_fpkm <- log2(dt$lsu_fpkm)
dt$log2_ssu_fpkm <- log2(dt$ssu_fpkm)
dt$log2_scanning_eff <- log2(dt$scanning_eff)
dt$log2_initiation_rate <- log2(dt$initiation_rate)
dt$log2_scanning_ratio <- log2(dt$scanning_ratio)
dt$log2_ssu_double <- log2(dt$ssu_double / dt$rna_fpkm)
dt$log2_ssu_queue <- log2(dt$ssu_queue)

dt <- dt[dt$log2_rna_fpkm > 1, ]

# # Arc
# a <- dt[dt$tx == "ENSMUST00000023268", ]
# a$tx <- NULL
# a <- melt(a, id.vars = "condition")
# dcast(a, variable ~ condition)

ggboxplot_by <- function(dtm, var = "log2_le_te", name = "leader TE [log2]", save_name = "leader_TE_vs_condition") {
  dtm$condition_group <- interaction(dtm$condition, dtm$group)
  dt2 <- dtm[, c("tx", "condition_group", var), with = F][!is.na(dtm[[var]]) & !is.infinite(dtm[[var]])]
  stat_test <- dt2[, add_significance(adjust_pvalue(t_test(
    .SD, as.formula(paste0(var, " ~ condition_group")), p.adjust.method = "none"), method = "BH"), "p.adj")]
  y_pos <- dt2[, get_y_position(.SD, as.formula(paste0(var," ~ condition_group")), scales = "free")]
  
  stat_test <- merge(stat_test, y_pos, by = c("group1", "group2"))
  ucg  <- unique(as.character(dtm$condition_group))
  ucg <- ucg[order(ucg)]
  dtm$condition_group <- factor(dtm$condition_group, ordered = T, levels = ucg, labels = ucg)
  
  # nicer ordering of statistics
  g1_pos <- match(stat_test$group1, ucg)
  g2_pos <- match(stat_test$group2, ucg)
  pos_diff <- abs(g1_pos - g2_pos)
  # the first one has to be the longest
  pos_min <- mapply(min, g1_pos, g2_pos)
  stat_test <- stat_test[order(pos_diff, pos_min), ]
  
  p <- ggplot(dtm[is.finite(dtm[[var]]), ], 
              aes(x = condition_group, y = get(var), color = condition_group)) +
    # geom_segment(data = dtm[is.finite(dtm[[var]]), ][
    #   , .(median = median(get(var), na.rm = T)), by = c("condition_group")],
    #   aes(y = median, yend = median, x = ucg[1], xend = ucg[length(ucg)],
    #       color = condition_group, linetype = condition_group), linewidth = 0.7) +
    geom_boxplot() +
    labs(x = "", y = name) +
    theme(legend.position = "none",
          axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
    stat_pvalue_manual(
      stat_test,
      step.increase = 0.06,
      tip.length = 0.01,
      bracket.nudge.y = 0.05,
      label = "p.adj.signif") +
    scale_color_manual(values = rxtras:::adj_colorblind_mine)
  as.pngpdf(paste0("figures/Clive/metrics/", save_name), p, width = 7, height = 10)
}

ggdensity_by <- function(dtm, var = "log2_le_te", name = "leader TE [log2]", 
                         save_name = "leader_TE_vs_condition_density") {
  dtm$condition_group <- interaction(dtm$condition, dtm$group)
  dt <- dtm[!is.na(dtm[[var]]) & !is.infinite(dtm[[var]]), ]
  p <- ggdensity(dt, x = var, fill = "condition_group") +
    labs(x = name, fill = "") + 
    # geom_vline(data = dt[, .(median = median(get(var), na.rm = T)), by = c("condition", "celltype")], 
    #   mapping = aes(xintercept = median, color = condition, linetype = condition)) +
    theme_science() +
    theme(plot.title = element_text(hjust = 0.5)) +
    scale_color_manual(values = rxtras:::adj_colorblind_mine) +
    scale_fill_manual(values = rxtras:::adj_colorblind_mine)
  as.pngpdf(paste0("figures/Clive/metrics/", save_name), p, width = 10, height = 6) 
}

for (t in unique(clive$title)) {
  clive_t <- clive[clive$title == t, ]
  dtm <- copy(dt)
  dtm$group <- clive_t$group[match(dtm$tx, clive_t$tx_id)]
  dtm <- dtm[!is.na(dtm$group), ]
  dtm <- dtm[dtm$group != "other", ]
  
  # lets plot SSU vs 80S
  p <- ggplot(dtm[is.finite(log2_ssu_fpkm) & is.finite(log2_lsu_fpkm), ], 
              aes(x = log2_lsu_fpkm, y = log2_ssu_fpkm)) +
    geom_point() + 
    stat_cor() +
    facet_grid(condition + group ~ .)  +
    labs(x = "80S [log2 FPKM]", y = "SSU [log2 FPKM]")
  as.pngpdf(paste0("figures/Clive/metrics/", t, "_ssu_vs_80S_by_condition"), p, width = 7, height = 6)
  
  p <- ggplot(dtm[is.finite(log2_ssu_fpkm) & is.finite(log2_lsu_fpkm), ], 
              aes(x = log2_lsu_fpkm, y = log2_ssu_fpkm, 
                  color = interaction(condition, group), 
                  group = interaction(condition, group))) +
    geom_smooth() +
    labs(x = "80S [log2 FPKM]", y = "SSU [log2 FPKM]", color = "") +
    theme_science() +
    scale_color_manual(values = rxtras:::adj_colorblind_mine)
  as.pngpdf(paste0("figures/Clive/metrics/", t, "_ssu_vs_80S_by_condition_smooth"), p, width = 7, height = 6)
  
  
  # lets plot TE vs RNA
  p <- ggplot(dtm[is.finite(log2_te)], 
              aes(x = rna_fpkm, y = log2_te)) +
    geom_point() +
    facet_grid(condition ~ group) +
    coord_cartesian(xlim = c(0, 1000), 
                    ylim = c(-max(dtm$log2_te[is.finite(dt$log2_te)]), 
                             max(dtm$log2_te[is.finite(dt$log2_te)]))) +
    labs(x = "RNA [zoomed to 1k FPKM]", y = "TE [log2]")
  as.pngpdf(paste0("figures/Clive/metrics/", t, "_TE_vs_RNA_by_condition"), p, width = 7, height = 6)
  
  p <- ggplot(dtm[is.finite(log2_scanning_eff)], 
              aes(x = rna_fpkm, y = log2_scanning_eff)) +
    geom_point() +
    facet_grid(condition ~ group) +
    coord_cartesian(xlim = c(0, 1000),
                    ylim = c(-max(dtm$log2_scanning_eff[is.finite(dt$log2_scanning_eff)]), 
                             max(dtm$log2_scanning_eff[is.finite(dt$log2_scanning_eff)]))) +
    labs(x = "RNA [zoomed to 1k FPKM]", y = "Scaning Efficiency [log2]")
  as.pngpdf(paste0("figures/Clive/metrics/", t, "_SE_vs_RNA_by_condition_zoom"), p, width = 7, height = 6)
  
  ggdensity_by(dtm, save_name = paste0(t, "_leader_TE_vs_condition_density")) 
  ggboxplot_by(dtm, save_name = paste0(t, "_leader_TE_vs_condition"))
  
  ggboxplot_by(dtm, "log2_rna_fpkm", "RNA FPKM [log2]", paste0(t, "_rna_vs_condition"))
  ggdensity_by(dtm, "log2_rna_fpkm", "RNA FPKM [log2]", paste0(t, "_rna_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_ssu_fpkm", "SSU FPKM [log2]", paste0(t, "_ssu_vs_condition"))
  ggdensity_by(dtm, "log2_ssu_fpkm", "SSU FPKM [log2]", paste0(t, "_ssu_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_lsu_fpkm", "80S FPKM [log2]", paste0(t, "_lsu_vs_condition"))
  ggdensity_by(dtm, "log2_lsu_fpkm", "80S FPKM [log2]", paste0(t, "_lsu_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_te", "TE [log2]", paste0(t, "_TE_vs_condition"))
  ggdensity_by(dtm, "log2_te", "TE [log2]", paste0(t, "_TE_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_scanning_eff", "Scanning efficiency [log2]", 
               paste0(t, "_scanning_eff_vs_condition"))
  ggdensity_by(dtm, "log2_scanning_eff", "Scanning efficiency [log2]", 
               paste0(t, "_scanning_eff_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_scanning_ratio", "Scanning ratio [log2]", 
               paste0(t, "_scanning_ratio_vs_condition"))
  ggdensity_by(dtm, "log2_scanning_ratio", "Scanning ratio [log2]", 
               paste0(t, "_scanning_ratio_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_ssu_double", "SSU double / RNA [log2]", 
               paste0(t, "_ssu_double_vs_condition"))
  ggdensity_by(dtm, "log2_ssu_double", "SSU double / RNA [log2]", 
               paste0(t, "_ssu_double_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_ssu_queue", "SSU queue [log2]", 
               paste0(t, "_ssu_queue_vs_condition"))
  ggdensity_by(dtm, "log2_ssu_queue", "SSU queue [log2]", 
               paste0(t, "_ssu_queue_vs_condition_density"))
  
  ggboxplot_by(dtm, "log2_initiation_rate", "Initiation rate [log2]", 
               paste0(t, "_initiation_rate_vs_condition"))
  ggdensity_by(dtm, "log2_initiation_rate", "Initiation rate [log2]", 
               paste0(t, "_initiation_rate_vs_condition_density"))
  
  ggboxplot_by(dtm, "entropy", "Entropy", 
               paste0(t, "_entropy_vs_condition"))
  ggdensity_by(dtm, "entropy", "Entropy", 
               paste0(t, "_entropy_vs_condition_density"))
}