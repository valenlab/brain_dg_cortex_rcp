#!/bin/bash

# Set errexit to stop on errors
set -e

# Define variables for tools (assuming they are in PATH or BT2_HOME is set)
BT2_CMD="bowtie2"
SAMTOOLS_CMD="samtools"

# Check if required number of arguments is provided
if [ "$#" -ne 4 ]; then
  echo "Usage: $0 <wdir> <bowtie2_index> <name> <output_file>"
  exit 1
fi

# Assign input parameters to variables
WDIR="$1"
BOWTIE_IDX="$2"
NAME="$3"
OUTPUT_FILE="$4"

# Check if wdir exists and is a directory
if [ ! -d "$WDIR" ]; then
  echo "Error: wdir '$WDIR' is not a directory or does not exist."
  exit 1
fi

find "$WDIR" -maxdepth 1 -name "*.fastq.gz" -print0 | while IFS= read -r -d $'\0' input_file; do
  OUTPUT_SAM=$(mktemp)
  echo "Processing file: $input_file"
  MAPPED_PER=$( "$BT2_CMD" -x "$BOWTIE_IDX" -U "$input_file" -S "$OUTPUT_SAM" 2>&1 | awk '/overall alignment rate/ { gsub(/%/,"",$1); print $1 }' )
  rm "$OUTPUT_SAM"
  
  INPUT_BASENAME=$(basename $input_file)
  echo "$INPUT_BASENAME $NAME $MAPPED_PER" >> "$OUTPUT_FILE"
done
exit 0