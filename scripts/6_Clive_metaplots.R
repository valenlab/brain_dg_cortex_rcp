rm(list = ls(all.names = TRUE))
gc(reset = TRUE)

library(ORFik)
library(data.table)

source("./scripts/utils.R")
ppath <- "/export/valenfs/data/processed_data/RCP-seq/Brain_DG_Cortex_Preeti/"
cfg <- read.experiment("Preeti_Brain_DG_Cortex_04_2023")

tx_filt <- fread("tables/filtered_tx.csv")$tx_filt
clive <- clive_groupings()

mrna <- countTable(cfg, "mrna", type = "fpkm", collapse = T, count.folder = "pshifted")
above1fpkm <- unique(rownames(mrna)[mrna$RNA_cortex > 1 | mrna$RNA_DG > 1])
tx_filt <- fread("tables/filtered_tx.csv")$tx_filt
tx_filt <- tx_filt[tx_filt %in% above1fpkm]

# lets make some metaplots for genes by category
load("data/coverage_for_metaplots.rds") # cov and rcount for TPM
cov <- cov[cov$genes %in% tx_filt, ]

# #cov[, c("libtype", "condition", "rep") := tstrsplit(fraction, "_"), ]
# gcount2 <- data.table::melt(gcount)
# colnames(gcount2) <- c("tx_id", "fraction", "count_by_gene")
# gcount2 <- as.data.table(gcount2)
# # we rename RNA to be SSU and LSU
# gcount2[, c("libtype", "condition", "rep") := tstrsplit(fraction, "_"), ]
# gcount2 <- gcount2[gcount2$libtype == "RNA", ]
# gcount_SSU <- gcount2
# gcount_LSU <- gcount2
# gcount_SSU$libtype <- "SSU"
# gcount_LSU$libtype <- "80S"
# gcount_LSU[, fraction := paste(libtype, condition, rep, sep = "_")]
# gcount_SSU[, fraction := paste(libtype, condition, rep, sep = "_")]
# gcount2 <- rbind(gcount_LSU, gcount_SSU)
# gcount2$count_by_gene <- as.double(gcount2$count_by_gene)
# # because we divide by total RNA we will also normalize to total RNA
# gcount2[, count_by_gene := count_by_gene * (10e6 / sum(count_by_gene)), by = c("fraction")]


titles <- unique(clive$title)
for (i in seq_along(titles)) {
  clive_i <- clive[clive$title == titles[i], ]
  clive_dt <- data.table::merge.data.table(clive_i, cov, by.x = "tx_id", by.y = "genes", all.x = F, all.y = F)
  clive_dt <- clive_dt[complete.cases(clive_dt), ]
  
  if (i == 9) {
    clive_dt <- clive_dt[!clive_dt$gene_id %in% c("ENSMUSG00000055430", "ENSMUSG00000029309"), ]
    # these two genes make the plot look ugly...
  }
  
  gene_count <- clive_dt[, .(gene_count = length(unique(tx_id))), by = c("fraction", "group")]
  
  # Figuring out which genes make the plot ugly - not my idea
  # # ENSMUSG00000029309
  # ok <- c("ENSMUSG00000029309", "ENSMUSG00000055430", "ENSMUSG00000033615")
  # a <- clive_dt[clive_dt$feature == "cds", ]
  # a <- a[a$position %in% 20:35 & a$gene_id != "ENSMUSG00000055430", ]
  # a <- pivot_wider(a, id_cols = c("group", "gene_id", "fraction"), names_from = "position", values_from = "score")
  # a <- a[!a$gene_id %in% ok, ]
  # a$div <- (a$`29` + 1) / (a$`25` + 1)
  # a[a$gene_id != "ENSMUSG00000055430", 
  #   .(score = sum(score)), by = c("fraction", "position")][, .(pos = position[which.max(score)]), by = "fraction"]
  # a[!a$gene_id %in% ok, .(gene_id = gene_id[which.max(score)]), by = "fraction"]
  
  # cov2 <- merge.data.table(clive_dt, gcount2, by = c("tx_id", "fraction"), all = F)
  # cov2[, score_norm := score/count_by_gene]
  
  # TPM
  clive_dt_ <- clive_dt[, .(score = sum(score, na.rm = TRUE)), 
                   by = c("fraction", "position", "feature", "group")]
  clive_dt_$total <- rcount[match(clive_dt_$fraction, names(rcount))]
  clive_dt_$score <- clive_dt_$score * (10e6 / clive_dt_$total)
  
  clive_dt_ <- merge(clive_dt_, gene_count)
  clive_dt_$score <- clive_dt_$score / clive_dt_$gene_count
  clive_dt_[, c("libtype", "condition", "rep") := tstrsplit(fraction, "_"), ]
  cov_plot <- clive_dt_[, .(score = mean(score[is.finite(score)], na.rm = T)), 
                        by = c("position", "feature", "libtype", "condition", "group")]
  cov_plot <- factorize(cov_plot)
  p <- metaplot3(cov_plot, xlab = "TPM")
  as.pngpdf(file.path(ppath, "figures", "Clive", paste0(titles[i], "_metaplot_TPM")), plot = p, height = 7)
  
  # txNorm
  clive_dt[, `:=`(tx_norm = sum(score)), by = c("fraction", "tx_id")]
  clive_dt$tx_norm <- clive_dt$score / clive_dt$tx_norm
  clive_dt$tx_norm[!is.finite(clive_dt$tx_norm)] <- NA
  # now we take the mean for each position reducing genes
  clive_dt_ <- clive_dt[, .(score = mean(tx_norm, na.rm = T)), 
                        by = c("position", "feature", "fraction", "group")]
  clive_dt_[, c("libtype", "condition", "rep") := tstrsplit(fraction, "_"), ]
  clive_dt_ <- factorize(clive_dt_)
  cov_plot <- clive_dt_[, .(score = mean(score, na.rm = T)), 
                        by = c("position", "feature", "libtype", "condition", "group")]
  p <- metaplot3(cov_plot)
  as.pngpdf(file.path(ppath, "figures", "Clive", paste0(titles[i], "_metaplot_tx_norm")), plot = p, height = 7)
  
  # zscore
  clive_dt[, `:=`(geneSD = sd(score, na.rm = T),
                  geneMean = mean(score, na.rm = T)), by = c("fraction", "tx_id")]
  clive_dt[, `:=`(zscore = (score - geneMean)/geneSD)]
  # now we take the mean for each position reducing genes
  clive_dt_ <- clive_dt[, .(score = mean(zscore, na.rm = T)), by = c("position", "feature", "fraction", "group")]
  clive_dt_[, c("libtype", "condition", "rep") := tstrsplit(fraction, "_"), ]
  clive_dt_ <- factorize(clive_dt_)
  cov_plot <- clive_dt_[, .(score = mean(score, na.rm = T)), 
                        by = c("position", "feature", "libtype", "condition", "group")]
  p <- metaplot3(cov_plot, xlab = "zscore")
  as.pngpdf(file.path(ppath, "figures", "Clive", paste0(titles[i], "_metaplot_zscore")), plot = p, height = 7)
}


# because reviewer asked we make the same scale for 
# neuronal_somata_monosome_vs_polysome
# neuronal_neuropil_monosome_vs_polysome

i = which(titles == "neuronal_somata_monosome_vs_polysome")
j = which(titles == "neuronal_neuropil_monosome_vs_polysome")
clive_i <- clive[clive$title == titles[i], ]
clive_j <- clive[clive$title == titles[j], ]
clive_dt <- data.table::merge.data.table(clive_i, cov, by.x = "tx_id", by.y = "genes", all.x = F, all.y = F)
clive_dt <- clive_dt[complete.cases(clive_dt), ]
clive_dtj <- data.table::merge.data.table(clive_j, cov, by.x = "tx_id", by.y = "genes", all.x = F, all.y = F)
clive_dtj <- clive_dtj[complete.cases(clive_dtj), ]

gene_count <- clive_dt[, .(gene_count = length(unique(tx_id))), by = c("fraction", "group")]
gene_countj <- clive_dtj[, .(gene_count = length(unique(tx_id))), by = c("fraction", "group")]


# TPM
clive_dt_ <- clive_dt[, .(score = sum(score, na.rm = TRUE)), 
                      by = c("fraction", "position", "feature", "group")]
clive_dt_$total <- rcount[match(clive_dt_$fraction, names(rcount))]
clive_dt_$score <- clive_dt_$score * (10e6 / clive_dt_$total)

clive_dt_ <- merge(clive_dt_, gene_count)
clive_dt_$score <- clive_dt_$score / clive_dt_$gene_count
clive_dt_[, c("libtype", "condition", "rep") := tstrsplit(fraction, "_"), ]
cov_plot <- clive_dt_[, .(score = mean(score, na.rm = T)), 
                      by = c("position", "feature", "libtype", "condition", "group")]
cov_plot <- factorize(cov_plot)


clive_dt_j <- clive_dtj[, .(score = sum(score, na.rm = TRUE)), 
                        by = c("fraction", "position", "feature", "group")]
clive_dt_j$total <- rcount[match(clive_dt_j$fraction, names(rcount))]
clive_dt_j$score <- clive_dt_j$score * (10e6 / clive_dt_j$total)

clive_dt_j <- merge(clive_dt_j, gene_countj)
clive_dt_j$score <- clive_dt_j$score / clive_dt_j$gene_count
clive_dt_j[, c("libtype", "condition", "rep") := tstrsplit(fraction, "_"), ]
cov_plotj <- clive_dt_j[, .(score = mean(score, na.rm = T)), 
                      by = c("position", "feature", "libtype", "condition", "group")]
cov_plotj <- factorize(cov_plotj)


library(ggh4x)
p <- metaplot3(cov_plot, xlab = "TPM")

df_scales <- data.frame(
  panel= c("SSU", "80S", "RNA"),
  ymin = rep(0, 3),
  ymax = c(300, 1200, 200),
  n = rep(4, 3)
)
df_scales <- split(df_scales, df_scales$panel)

scales <- lapply(df_scales, function(x) {
  scale_y_continuous(limits = c(x$ymin, x$ymax), n.breaks = x$n)
})
p <- p + ggh4x::facetted_pos_scales(y = scales)
as.pngpdf(file.path(ppath, "figures", "Clive", paste0(titles[i], "_metaplot_TPM")), plot = p, height = 7)

pj <- metaplot3(cov_plotj, xlab = "TPM")
pj <- pj + ggh4x::facetted_pos_scales(y = scales)
as.pngpdf(file.path(ppath, "figures", "Clive", paste0(titles[j], "_metaplot_TPM")), plot = pj, height = 7)
